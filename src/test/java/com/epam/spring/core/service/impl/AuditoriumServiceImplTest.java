package com.epam.spring.core.service.impl;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataAccessException;

import com.epam.spring.core.dao.AuditoriumDao;
import com.epam.spring.core.domain.Auditorium;

@RunWith(MockitoJUnitRunner.class)
public class AuditoriumServiceImplTest {
	@InjectMocks
	private AuditoriumServiceImpl auditoriumService;
	@Mock
	private AuditoriumDao auditoriumDao;

	private final Long expectedAuditoriumId = 5L;
	private final long expectedAuditoriumAmount = 10;
	private final String expectedAuditoriumName = "Minsk-arena";

	@Test
	public void getAllTest() {
		final Collection<Auditorium> auditoriums = new ArrayList<Auditorium>();
		for (int i = 0; i < expectedAuditoriumAmount; i++) {
			auditoriums.add(new Auditorium());
		}
		doReturn(auditoriums).when(auditoriumDao).getAll();
		assertEquals(expectedAuditoriumAmount, auditoriumService.getAll()
				.size());
	}

	@Test(expected = DataAccessException.class)
	public void getAllFailTest() {
		doThrow(throwDataAccessException()).when(auditoriumDao).getAll();
		auditoriumService.getAll();
	}

	@Test
	public void getByNameTest() {
		final Collection<Auditorium> auditoriums = new ArrayList<Auditorium>();
		Auditorium auditorium = new Auditorium();
		auditorium.setName(expectedAuditoriumName);
		auditoriums.add(auditorium);
		doReturn(auditoriums).when(auditoriumDao).getAll();
		assertNotNull(auditoriumService.getByName(expectedAuditoriumName));
	}

	@Test
	public void saveTest() {
		when(auditoriumDao.save(any(Auditorium.class))).thenReturn(
				expectedAuditoriumId);
		Auditorium auditorium = new Auditorium();
		Long id = auditoriumService.save(auditorium);
		verify(auditoriumDao).save(auditorium);
		assertEquals(expectedAuditoriumId, id);
	}

	@Test(expected = DataAccessException.class)
	public void saveNullTest() {
		doThrow(throwDataAccessException()).when(auditoriumDao).save(null);
		auditoriumService.save(null);
	}

	@Test(expected = DataAccessException.class)
	public void saveFailTest() {
		doThrow(throwDataAccessException()).when(auditoriumDao).save(any());
		Auditorium auditorium = new Auditorium();
		auditoriumService.save(auditorium);
	}

	@Test
	public void removeTest() {
		doNothing().when(auditoriumDao).remove(any());
		Auditorium auditorium = new Auditorium();
		auditorium.setId(expectedAuditoriumId);
		auditoriumService.remove(auditorium);
		verify(auditoriumDao).remove(auditorium);
	}

	@Test(expected = DataAccessException.class)
	public void removeNullTest() {
		doThrow(throwDataAccessException()).when(auditoriumDao).remove(null);
		auditoriumService.remove(null);
	}
	
	@Test(expected = DataAccessException.class)
	public void removeFailTest() {
		doThrow(throwDataAccessException()).when(auditoriumDao).remove(any());
		auditoriumService.remove(new Auditorium());
	}
	
	@SuppressWarnings("serial")
	private DataAccessException throwDataAccessException() {
		DataAccessException dataAccessException = new DataAccessException(
				"application attempts to use null or exception occurred.") {
		};
		return dataAccessException;
	}
}
