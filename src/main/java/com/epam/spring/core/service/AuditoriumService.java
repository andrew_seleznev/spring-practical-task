package com.epam.spring.core.service;

import java.time.LocalDateTime;
import java.util.NavigableMap;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.epam.spring.core.domain.Auditorium;

/**
 * @author Yuriy_Tkach
 */
public interface AuditoriumService extends AbstractDomainObjectService<Auditorium> {

    /**
     * Getting all auditoriums from the system
     * 
     * @return set of all auditoriums
     */
//    @Nonnull Set<Auditorium> getAll();
    
    @Nonnull Set<Auditorium> getInitAuditoriums();
    /**
     * Finding auditorium by name
     * 
     * @param name
     *            Name of the auditorium
     * @return found auditorium or <code>null</code>
     */
    @Nullable Auditorium getByName(@Nonnull String name);
    
    NavigableMap<LocalDateTime, Auditorium> getAllEventAuditoriums(Long eventId);
    

}
