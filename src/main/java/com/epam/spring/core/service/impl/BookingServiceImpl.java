package com.epam.spring.core.service.impl;

import java.time.LocalDateTime;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.epam.spring.core.dao.BookingDao;
import com.epam.spring.core.dao.UserDao;
import com.epam.spring.core.domain.Auditorium;
import com.epam.spring.core.domain.Event;
import com.epam.spring.core.domain.Ticket;
import com.epam.spring.core.domain.User;
import com.epam.spring.core.service.AuditoriumService;
import com.epam.spring.core.service.BookingService;
import com.epam.spring.core.service.DiscountService;
import com.epam.spring.core.service.UserService;

@Service("bookingService")
public class BookingServiceImpl implements BookingService {

	static Logger logger = LogManager.getLogger(BookingServiceImpl.class);
	@Autowired
	@Qualifier("bookingDao")
	private BookingDao bookingDao;

	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;

	@Autowired
	@Qualifier("discountService")
	private DiscountService discountService;

	@Autowired
	@Qualifier("auditoriumService")
	private AuditoriumService auditoriumService;

	@Autowired
	@Qualifier("userService")
	private UserService userService;

	@Value("${vip.ticket.mark.up}")
	private Double vipTicketMarkUp;

	@Value("${high.rated.movie.mark.up}")
	private Double highRatedMovieMarkUp;

	@Value("${mid.rated.movie.mark.up}")
	private Double midRatedMovieMarkUp;

	@Override
	public double getTicketsPrice(Event event, LocalDateTime dateTime, User user, Set<Long> seats) {
		double ticketsPrice = 0;
		double currentPrice = 0;
		switch (event.getRating()) {
		case LOW:
			currentPrice = event.getBasePrice();
			break;
		case MID:
			currentPrice = event.getBasePrice() * midRatedMovieMarkUp;
			break;
		case HIGH:
			currentPrice = event.getBasePrice() * highRatedMovieMarkUp;
			break;
		default:
			System.err.println("Unknown action");
		}

		Auditorium auditorium = auditoriumService.getByName(event.getAuditoriums().get(dateTime).getName());
		if (auditorium != null) {
			for (Long seat : seats) {
				if (auditorium.getVipSeats().contains(seat)) {
					ticketsPrice += currentPrice * vipTicketMarkUp;
				} else {
					ticketsPrice += currentPrice;
				}
			}
		} else {
			System.err.println("Undefined required auditorium for event");
		}
		ticketsPrice -= ticketsPrice * discountService.getDiscount(user, event, dateTime, seats.size()) / 100;
		return ticketsPrice;
	}

	@Override
	public void bookTickets(Set<Ticket> tickets) {
		bookingDao.bookTickets(tickets);
		for (Ticket ticket : tickets) {
			User user = ticket.getUser();
			if (user != null) {
				user.getTickets().add(ticket);
			}
		}
	}

	@Override
	public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) {
		Set<Ticket> tickets = userDao.getAllEventTickets(event, dateTime);
		return tickets;
	}

}
