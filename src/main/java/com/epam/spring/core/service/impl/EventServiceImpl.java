package com.epam.spring.core.service.impl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.spring.core.dao.EventDao;
import com.epam.spring.core.domain.Auditorium;
import com.epam.spring.core.domain.Event;
import com.epam.spring.core.domain.EventRating;
import com.epam.spring.core.service.AuditoriumService;
import com.epam.spring.core.service.EventService;

@Service("eventService")
public class EventServiceImpl implements EventService {

	static Logger logger = LogManager.getLogger(EventServiceImpl.class);

	@Autowired
	@Qualifier("eventDao")
	private EventDao eventDao;
	@Autowired
	@Qualifier("auditoriumService")
	private AuditoriumService auditoriumService;

	@Override
	@Transactional
	public Long save(Event object) {
		Long eventId = eventDao.save(object);
		for (Map.Entry<LocalDateTime, Auditorium> entry : object.getAuditoriums().entrySet()) {
			eventDao.saveEventAuditorium(eventId, entry.getValue().getId(), entry.getKey());
		}
		return eventId;
	}

	@Override
	public void remove(Event object) {
		eventDao.remove(object);
	}

	@Override
	public Event getById(Long id) {
		Event event = eventDao.getById(id);
		event.setAuditoriums(auditoriumService.getAllEventAuditoriums(event.getId()));
		event.setAirDates(auditoriumService.getAllEventAuditoriums(event.getId()).navigableKeySet());
		return event;
	}

	@Override
	public Collection<Event> getAll() {
		Collection<Event> events = eventDao.getAll();
		for (Event event : events) {
			event.setAuditoriums(auditoriumService.getAllEventAuditoriums(event.getId()));
			event.setAirDates(auditoriumService.getAllEventAuditoriums(event.getId()).navigableKeySet());
		}
		return events;
	}

	@Override
	public Event getByName(String name) {
		Event event = eventDao.getByName(name);
		event.setAuditoriums(auditoriumService.getAllEventAuditoriums(event.getId()));
		event.setAirDates(auditoriumService.getAllEventAuditoriums(event.getId()).navigableKeySet());
		return event;
	}

	@Override
	public Long saveEventRating(EventRating eventRating) {
		Long eventRatingId = eventDao.saveEventRating(eventRating);
		return eventRatingId;
	}

}
