package com.epam.spring.core.service.impl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.spring.core.dao.AuditoriumDao;
import com.epam.spring.core.domain.Auditorium;
import com.epam.spring.core.service.AuditoriumService;

@Service("auditoriumService")
public class AuditoriumServiceImpl implements AuditoriumService {

	static Logger logger = LogManager.getLogger(AuditoriumServiceImpl.class);
	@Resource(name = "auditoriums")
	private Set<Auditorium> auditoriums;
	@Autowired
	@Qualifier("auditoriumDao")
	private AuditoriumDao auditoriumDao;
	
	@Override
	public Collection<Auditorium> getAll() {
		return auditoriumDao.getAll();
	}

	@Override
	public Auditorium getByName(String name) {
		Optional<Auditorium> auditorium = getAll().stream().filter(a -> (a.getName().equals(name))).findFirst();
		return (auditorium.isPresent()) ? auditorium.get() : null;
	}

	@Override
	@Transactional
	public Long save(Auditorium object) {
		Long auditoriumId = auditoriumDao.save(object);
		for (Long vipSeat : object.getVipSeats()) {
			auditoriumDao.saveAuditoriumVipSeat(auditoriumId, vipSeat);
		}
		return auditoriumId;
	}

	@Override
	public void remove(Auditorium object) {
		auditoriumDao.remove(object);
	}

	@Override
	public Auditorium getById(Long id) {
		Auditorium auditorium = auditoriumDao.getById(id);
		return auditorium;
	}

	@Override
	public Set<Auditorium> getInitAuditoriums() {
		return auditoriums;
	}

	@Override
	public NavigableMap<LocalDateTime, Auditorium> getAllEventAuditoriums(Long eventId) {
		NavigableMap<LocalDateTime, Auditorium> map = auditoriumDao.getAllEventAuditoriums(eventId);
		return map;
	}

}
