package com.epam.spring.core.service.impl;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.epam.spring.core.dao.EventDao;
import com.epam.spring.core.dao.UserDao;
import com.epam.spring.core.domain.Ticket;
import com.epam.spring.core.domain.User;
import com.epam.spring.core.service.AuditoriumService;
import com.epam.spring.core.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	static Logger logger = LogManager.getLogger(UserServiceImpl.class);
	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;
	@Autowired
	@Qualifier("eventDao")
	private EventDao eventDao;
	@Autowired
	@Qualifier("auditoriumService")
	private AuditoriumService auditoriumService;

	@Override
	public Long save(User object) {
		Long userId = userDao.save(object);
		return userId;
	}

	@Override
	public void remove(User object) {
		userDao.remove(object);
	}

	@Override
	public User getById(Long id) {
		User user = userDao.getById(id);
		user.setTickets(eventDao.getAllUserTickets(user));
		for (Ticket ticket : user.getTickets()) {
			ticket.getEvent().setAuditoriums(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()));
			ticket.getEvent()
					.setAirDates(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()).navigableKeySet());
		}
		return user;
	}

	@Override
	public Collection<User> getAll() {
		Collection<User> users = userDao.getAll();
		for (User user : users) {
			user.setTickets(eventDao.getAllUserTickets(user));
			for (Ticket ticket : user.getTickets()) {
				ticket.getEvent().setAuditoriums(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()));
				ticket.getEvent().setAirDates(
						auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()).navigableKeySet());
			}
		}
		return users;
	}

	@Override
	public User getUserByEmail(String email) {
		User user = userDao.getUserByEmail(email);
		user.setTickets(eventDao.getAllUserTickets(user));
		for (Ticket ticket : user.getTickets()) {
			ticket.getEvent().setAuditoriums(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()));
			ticket.getEvent()
					.setAirDates(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()).navigableKeySet());
		}
		return user;
	}

}
