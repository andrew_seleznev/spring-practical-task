package com.epam.spring.core.service;

import com.epam.spring.core.domain.Counter;
import com.epam.spring.core.domain.CountingCriteria;

public interface CounterService extends AbstractDomainObjectService<Counter> {
	
	Counter getByCountingCriteria (CountingCriteria countingCriteria, Long countedObjectId);
	
	void updateCounter (Counter counter);
}
