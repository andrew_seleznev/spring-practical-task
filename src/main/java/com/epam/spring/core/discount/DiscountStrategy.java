package com.epam.spring.core.discount;

import java.time.LocalDate;

import com.epam.spring.core.domain.Event;
import com.epam.spring.core.domain.User;

public interface DiscountStrategy {
	
	byte getDiscount(User user, Event event, LocalDate airDate, long numberOfTickets);
	
}
