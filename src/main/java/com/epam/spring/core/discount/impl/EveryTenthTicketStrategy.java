package com.epam.spring.core.discount.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.epam.spring.core.discount.DiscountStrategy;
import com.epam.spring.core.domain.Event;
import com.epam.spring.core.domain.User;

@Component("everyTenthTicketStrategy")
public class EveryTenthTicketStrategy implements DiscountStrategy {
	
	@Value("${every.ten.discount}")
	private byte everyTenDiscount;
	
	@Override
	public byte getDiscount(User user, Event event, LocalDate airDate, long numberOfTickets) {
		byte discount = 0;
		if (user != null) {
			for (int i = 1; i <= numberOfTickets; i++) {
				if ((user.getTickets().size() + i) % 10 == 0) {
					discount += everyTenDiscount;
				}
			}
		} else if (numberOfTickets > 10) {
			for (int i = 1; i <= numberOfTickets; i++) {
				if (i % 10 == 0) {
					discount += everyTenDiscount;
				}
			}
		}
		return (byte) (discount / numberOfTickets);
	}

}
