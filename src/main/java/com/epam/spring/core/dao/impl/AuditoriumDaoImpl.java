package com.epam.spring.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.epam.spring.core.dao.AuditoriumDao;
import com.epam.spring.core.domain.Auditorium;

@Repository("auditoriumDao")
public class AuditoriumDaoImpl implements AuditoriumDao {

	private static final String SAVE_AUDITORIUM_SQL = "INSERT INTO AUDITORIUM (AUDITORIUM_NAME, AUDITORIUM_NUMBER_OF_SEATS) VALUES (?, ?)";
	private static final String SAVE_AUDITORIUM_VIP_SEAT_SQL = "INSERT INTO AUDITORIUM_VIP_SEAT (AUDITORIUM_ID, AUDITORIUM_SEAT) VALUES (?, ?)";
	private static final String FIND_ALL_AUDITORIUMS_SQL = "SELECT AUDITORIUM_ID, AUDITORIUM_NAME, AUDITORIUM_NUMBER_OF_SEATS FROM AUDITORIUM";
	private static final String FIND_VIP_SEATS_BY_ID = "SELECT AUDITORIUM_SEAT FROM AUDITORIUM_VIP_SEAT WHERE AUDITORIUM_ID = ?";
	private static final String FIND_AUDITORIUM_BY_ID = "SELECT AUDITORIUM_ID, AUDITORIUM_NAME, AUDITORIUM_NUMBER_OF_SEATS FROM AUDITORIUM WHERE AUDITORIUM_ID = ?";
	private static final String FIND_ALL_EVENT_AUDITORIUMS_SQL = "SELECT EVENT_DATE, AUDITORIUM_ID FROM EVENT_AUDITORIUM WHERE EVENT_ID = ?";
	private static final String REMOVE_AUDITORIUM_SQL = "DELETE FROM AUDITORIUM WHERE AUDITORIUM_ID = ?";
	private static final String AUDITORIUM_ID_COLUMN = "AUDITORIUM_ID";
	private static final String AUDITORIUM_VIP_SEAT_ID_COLUMN = "AUDITORIUM_VIP_SEAT_ID";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long save(Auditorium object) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { AUDITORIUM_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_AUDITORIUM_SQL, columns);
				preparedStatement.setString(1, object.getName());
				preparedStatement.setLong(2, object.getNumberOfSeats());
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void remove(Auditorium object) {
		jdbcTemplate.update(REMOVE_AUDITORIUM_SQL, object.getId());
	}

	@Override
	public Auditorium getById(Long id) {
		Auditorium auditorium = null;
		auditorium = jdbcTemplate.queryForObject(FIND_AUDITORIUM_BY_ID, new Object[] { id },
				new RowMapper<Auditorium>() {
					@Override
					public Auditorium mapRow(ResultSet rs, int rowNum) throws SQLException {
						Auditorium auditorium = new Auditorium();
						auditorium.setId(rs.getLong(1));
						auditorium.setName(rs.getString(2));
						auditorium.setNumberOfSeats(rs.getLong(3));
						auditorium.setVipSeats(getAllVipSeats(rs.getLong(1)));
						return auditorium;
					}
				});
		return auditorium;
	}

	@Override
	public Collection<Auditorium> getAll() {
		List<Auditorium> auditoriums = null;
		auditoriums = jdbcTemplate.query(FIND_ALL_AUDITORIUMS_SQL, new RowMapper<Auditorium>() {
			@Override
			public Auditorium mapRow(ResultSet rs, int rowNum) throws SQLException {
				Auditorium auditorium = new Auditorium();
				auditorium.setId(rs.getLong(1));
				auditorium.setName(rs.getString(2));
				auditorium.setNumberOfSeats(rs.getLong(3));
				auditorium.setVipSeats(getAllVipSeats(rs.getLong(1)));
				return auditorium;
			}

		});
		return auditoriums;
	}

	@Override
	public Long saveAuditoriumVipSeat(Long auditoriumId, Long vipSeat) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { AUDITORIUM_VIP_SEAT_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_AUDITORIUM_VIP_SEAT_SQL, columns);
				preparedStatement.setLong(1, auditoriumId);
				preparedStatement.setLong(2, vipSeat);
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	private Set<Long> getAllVipSeats(Long auditoriumId) {
		Set<Long> vipSeats = new HashSet<Long>();
		List<Long> list = jdbcTemplate.query(FIND_VIP_SEATS_BY_ID, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, auditoriumId);
			}
		}, new RowMapper<Long>() {
			@Override
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong(1);
			}
		});
		vipSeats.addAll(list);
		return vipSeats;
	}

	@Override
	public NavigableMap<LocalDateTime, Auditorium> getAllEventAuditoriums(Long eventId) {
		NavigableMap<LocalDateTime, Auditorium> map = new TreeMap<>();
		map = jdbcTemplate.query(FIND_ALL_EVENT_AUDITORIUMS_SQL, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, eventId);
			}
		}, new ResultSetExtractor<NavigableMap<LocalDateTime, Auditorium>>() {
			@Override
			public NavigableMap<LocalDateTime, Auditorium> extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NavigableMap<LocalDateTime, Auditorium> map = new TreeMap<>();
				while (rs.next()) {
					map.put(rs.getTimestamp(1).toLocalDateTime(), getById(rs.getLong(2)));
				}
				return map;
			}
		});
		return map;
	}
}
