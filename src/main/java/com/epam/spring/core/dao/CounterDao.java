package com.epam.spring.core.dao;

import com.epam.spring.core.domain.Counter;
import com.epam.spring.core.domain.CountingCriteria;

public interface CounterDao extends AbstractDomainObjectDao<Counter> {
	
	Counter getByCountingCriteria (CountingCriteria countingCriteria, Long countedObjectId);
	
	void updateCounter (Counter counter);
}
