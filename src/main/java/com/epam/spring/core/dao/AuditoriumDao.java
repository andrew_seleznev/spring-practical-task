package com.epam.spring.core.dao;

import java.time.LocalDateTime;
import java.util.NavigableMap;

import com.epam.spring.core.domain.Auditorium;

public interface AuditoriumDao extends AbstractDomainObjectDao<Auditorium>{

	Long saveAuditoriumVipSeat(Long auditoriumId, Long vipSeat);
	
	NavigableMap<LocalDateTime, Auditorium> getAllEventAuditoriums(Long eventId);
	
}
