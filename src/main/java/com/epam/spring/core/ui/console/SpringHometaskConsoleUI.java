package com.epam.spring.core.ui.console;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.epam.spring.core.config.AppConfig;
import com.epam.spring.core.domain.Auditorium;
import com.epam.spring.core.domain.Event;
import com.epam.spring.core.domain.EventRating;
import com.epam.spring.core.domain.Ticket;
import com.epam.spring.core.domain.User;
import com.epam.spring.core.service.AuditoriumService;
import com.epam.spring.core.service.BookingService;
import com.epam.spring.core.service.EventService;
import com.epam.spring.core.service.UserService;
import com.epam.spring.core.ui.console.state.MainState;

/**
 * Simple console UI application for the hometask code. UI provides different
 * action to input and output data. In order for the application to work, the
 * Spring context initialization code should be placed into
 * {@link #initContext()} method.
 * 
 * @author Yuriy_Tkach
 */
@Component
public class SpringHometaskConsoleUI {

    private AnnotationConfigApplicationContext context;

    public static void main(String[] args) {
        SpringHometaskConsoleUI ui = new SpringHometaskConsoleUI();
        ui.initContext();
        ui.run();
    }

    private void initContext() {
    	context = new AnnotationConfigApplicationContext();
    	context.register(AppConfig.class);
    	context.refresh();
    }

    private void run() {
        System.out.println("Welcome to movie theater console service");
        
        fillInitialData();

        MainState state = new MainState(context);

        state.run();

        System.out.println("Exiting.. Thank you.");
    }

    private void fillInitialData() {
        UserService userService = context.getBean("userService", UserService.class);
        EventService eventService = context.getBean("eventService", EventService.class);
        AuditoriumService auditoriumService = context.getBean("auditoriumService", AuditoriumService.class);
        BookingService bookingService = context.getBean("bookingService", BookingService.class);

        for(Auditorium auditorium : auditoriumService.getInitAuditoriums()) {
        	auditoriumService.save(auditorium);
        }
        
        Auditorium auditorium = auditoriumService.getAll().iterator().next();
        if (auditorium == null) {
            throw new IllegalStateException("Failed to fill initial data - no auditoriums returned from AuditoriumService");
        }
        
        if (auditorium.getNumberOfSeats() <= 0) {
            throw new IllegalStateException("Failed to fill initial data - no seats in the auditorium " + auditorium.getName());
        }
        User user = new User();
        user.setEmail("my@email.com");
        user.setFirstName("Foo");
        user.setLastName("Bar");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse("2016-05-12", formatter);
        user.setBirthDay(date);
        userService.save(user);
        Event event = new Event();
        event.setName("Grand concert");
        event.setRating(EventRating.MID);
        event.setBasePrice(10);
        LocalDateTime airDate = LocalDateTime.of(2016, 5, 15, 19, 30);
        event.addAirDateTime(airDate, auditorium);
        eventService.save(event);
        Ticket ticket1 = new Ticket(userService.getUserByEmail("my@email.com"), eventService.getByName("Grand concert"), airDate, 1);
        bookingService.bookTickets(Collections.singleton(ticket1));
//        if (auditorium.getNumberOfSeats() > 1) {
//            User userNotRegistered = new User();
//            userNotRegistered.setEmail("somebody@a.b");
//            userNotRegistered.setFirstName("A");
//            userNotRegistered.setLastName("Somebody");
//            userNotRegistered.setBirthDay(date);
//            userNotRegistered.setId(2L);
//            Ticket ticket2 = new Ticket(userNotRegistered, event, airDate, 2);
//            ticket2.setId(2L);
//            bookingService.bookTickets(Collections.singleton(ticket2));
//        }
        auditoriumService.save(null);
    }
}
